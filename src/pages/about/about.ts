import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Geocoder
} from '@ionic-native/google-maps';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  map: GoogleMap;
  mapElement: HTMLElement;

  constructor(public navCtrl: NavController, public navParams: NavParams,public platform:Platform, private googleMaps: GoogleMaps) {
    // platform.ready().then(() => {
    //
    //     this.loadMap();
    //
    //
    // });
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad AboutPage');
   this.loadMap()
  }

  goBack(){
     this.navCtrl.setRoot('HomePage');
  }

  loadMap() {

      this.mapElement = document.getElementById('map2');

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 35.771645,
          lng: -5.815576
        },
        zoom: 18,
        tilt: 0
      }
    };

    this.map = this.googleMaps.create(this.mapElement, mapOptions);

  }

}
