import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Geocoder
} from '@ionic-native/google-maps';
import { Component } from "@angular/core/";
import { IonicPage, Platform, NavController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  map: GoogleMap;
  mapElement: HTMLElement;

  constructor(private googleMaps: GoogleMaps, public platform: Platform, public navCtrl:NavController,private reverseGeocode : Geocoder,
  public modalCtrl:ModalController) {

    // Wait the native plugin is ready.
    // platform.ready().then(() => {
    //
    //     this.loadMap();
    //
    //
    // });

  }

ionViewDidEnter(){
  this.loadMap();
}

testButton(){

  // this.navCtrl.push('ContactPage')

  this.modalCtrl.create('AboutPage').present()

}


  loadMap() {

      this.mapElement = document.getElementById('map');

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 35.771645,
          lng: -5.815576
        },
        zoom: 18,
        tilt: 0
      }
    };

    this.map = this.googleMaps.create(this.mapElement, mapOptions);

  }

}
